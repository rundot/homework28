package ua.dp.rundot;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Created by rundot on 23.01.2017.
 */
public class ReflectionTest {

    public static void main(String[] args) throws Exception {
        Class exerciseClass = Class.forName("com.myclasses.Exercise");

        Object exercise = exerciseClass.newInstance();
        Field[] fields = exerciseClass.getDeclaredFields();
        Method[] methods = exerciseClass.getDeclaredMethods();

        //arguments = "refl3ction", 42L
        for (Field field : fields) {
            System.out.println("Field: " + field.getType().getCanonicalName() + " " + field.getName());
            boolean isAccessible = field.isAccessible();
            field.setAccessible(true);
            System.out.println("\tValue: " + field.get(exercise));
            field.setAccessible(isAccessible);
        }

        //method = String callMe(String string, Long long)
        for (Method method : methods) {
            System.out.println("Method: " + method.getName());
            Class[] paramTypes = method.getParameterTypes();
            for (Class paramType : paramTypes) {
                System.out.println("\tParam: " + paramType.getCanonicalName());
            }
            System.out.println("\tReturning value: " + method.getReturnType().getCanonicalName());
        }

        Method callMeMethod = exerciseClass.getDeclaredMethod("callMe", String.class, Long.class);
        boolean isAccessible = callMeMethod.isAccessible();
        callMeMethod.setAccessible(true);
        System.out.println("Calling method: " + callMeMethod.invoke(exercise, "refl3ction", 42L));
        callMeMethod.setAccessible(isAccessible);

        //Reflection is cool :)

    }

}
